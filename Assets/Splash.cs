﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Khadga.FreakSpace;

public class Splash : MonoBehaviour {

    public int Waiting_Time;

	// Use this for initialization
	void Start () {
        Screen.orientation = ScreenOrientation.LandscapeLeft; 
       StartCoroutine( Wait(Waiting_Time));
        SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[3].Name);
	}
	
    IEnumerator Wait(int x)
    {
        yield return new WaitForSeconds(x);
        SceneManager.LoadScene("Menu");
    }

	// Update is called once per frame
	void Update () {
		
	}
}
