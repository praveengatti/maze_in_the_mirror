﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Khadga.FreakSpace;

public class Menu : MonoBehaviour {

    public GameObject Options_Panel, Help_Panel;
    public GameObject Level_Select;

	// Use this for initialization
	void Start () {

        Options_Panel.SetActive(false);
        Help_Panel.SetActive(false);
        Level_Select.SetActive(false);
	}
	
    public void LoadLevel(string lvl_name)
    {
        SoundManager.Instance.PlayMusic(SoundManager.Instance.GameSounds[5]);
        SceneManager.LoadScene(lvl_name);
        PopUps_Script.Instance.pause_Button.SetActive(true);
    }

    public void BttonClick()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[1].Name);
    }

    public void open(GameObject GO)
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[2].Name);
        GO.SetActive(true);
    }

    public void close(GameObject GO)
    {
        GO.SetActive(false);
    }

    public void mute_UnMute()
    {
        BttonClick();
        SoundManager.Instance.ToggleMute();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
