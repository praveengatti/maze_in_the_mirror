﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Khadga.Utility;

public class MazeGenerator : MonoBehaviour
{
	public GameObject wall;
	public GameObject GenerationPlane; // this is the parent of all the walls going to be generated
	public int Width, Height;
	private int[,] Maze;
	private List<Vector3> pathMazes = new List<Vector3> ();
	
	private static MazeGenerator instance;
	public static MazeGenerator Instance {
		get {
			return instance;
		}
	}
	
	void Awake ()
	{
		instance = this;
	}
	
	void Start ()
	{
		GenerateMaze (MazeUtil. CreateMaze(Width,Height));
	}
	
	void GenerateMaze (int[,] maze)
	{
		
		GameObject ptype = null;
		
		for (int i = 0; i <= maze.GetUpperBound(0); i++) {
			for (int j = 0; j <= maze.GetUpperBound(1); j++) {
				if (maze [i, j] == 1) {
					ptype = wall;
					var x = (GameObject)Instantiate (wall, new Vector3 (i * ptype.transform.localScale.x, 1, j * ptype.transform.localScale.z), Quaternion.identity);
                    
					x.transform.SetParent (GenerationPlane.transform, true);
				} else if (maze [i, j] == 0) {
					pathMazes.Add (new Vector3 (i, j, 0));
				}
				
			}
		}
	}
	
	
}