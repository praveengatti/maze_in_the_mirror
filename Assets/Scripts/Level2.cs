﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.FreakSpace;

public class Level2 : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PopUps_Script.Instance.GameOver_PopUp.SetActive(true);
            PopUps_Script.Instance.pause_Button.SetActive(false);
            SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[4].Name);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
