﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Khadga.FreakSpace;

public class Level1 : MonoBehaviour {
    public float CompletionTime;
    bool IsgameCompleted=false;
    bool IsPopUpShown = false;
    public Text TimeText;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag =="Exit")
        {
            SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[6].Name);
            IsgameCompleted = true;
            GameoverPopUp();
            Debug.Log("dead");
        }
    }

    public void GameoverPopUp()
    {
        if (!IsPopUpShown)
        {
            Debug.Log("displaying popoup");
            PopUps_Script.Instance.pause_Button.SetActive(false);
            PopUps_Script.Instance.GameCompleted_PupUp.SetActive(true);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        TimeText.text = "Time Left:" +Mathf.Floor(CompletionTime);

        if (CompletionTime>=0&&!IsgameCompleted)
        {
            CompletionTime -= Time.deltaTime;
            Debug.Log(CompletionTime);
        }
        else if(!IsPopUpShown&&!IsgameCompleted)
        {
            SoundManager.Instance.PlaySound((SoundManager.Instance.GameSounds[4]).Name);
            IsPopUpShown = true;
            PopUps_Script.Instance.GameOver_PopUp.SetActive(true);
            PopUps_Script.Instance.pause_Button.SetActive(false);
        }
	}
}
