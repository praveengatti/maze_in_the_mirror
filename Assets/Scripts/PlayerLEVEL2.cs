﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.FreakSpace;

public class PlayerLEVEL2 : MonoBehaviour {

    // Use this for initialization
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Exit")
        {
            GameoverPopUp();
        }
    }

    public void GameoverPopUp()
    {
        Debug.Log("displaying popoup");
        SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[6].Name);
        PopUps_Script.Instance.pause_Button.SetActive(false);
        PopUps_Script.Instance.GameCompleted_PupUp.SetActive(true);
    }
}
