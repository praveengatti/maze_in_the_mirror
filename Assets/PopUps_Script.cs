﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Khadga.FreakSpace;


public class PopUps_Script : MonoBehaviour {

    private static PopUps_Script instance;
    public GameObject GameCompleted_PupUp, GameOver_PopUp, Pause_PopUp;
    public GameObject pause_Button;

    // Game Instance Singleton
    public static PopUps_Script Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void Pause()
    {
        common_function(0, true, false);
    }
    
    public void resume()
    {
        common_function(1,false,true);
    }

    public void LoadLevel(string str)
    {
        SceneManager.LoadScene(str);
        common_function(1, false, false);
       
    }

    public void Retry()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        common_function(1, false, true);
    }

    void common_function(int x,bool a,bool b)
    {
        Time.timeScale = x;
        Pause_PopUp.SetActive(a);
        pause_Button.SetActive(b);
        GameOver_PopUp.SetActive(false);
        GameCompleted_PupUp.SetActive(false);
    }
    public void BttonClick()
    {
        SoundManager.Instance.PlaySound(SoundManager.Instance.GameSounds[1].Name);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
