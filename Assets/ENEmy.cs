﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ENEmy : MonoBehaviour {
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PopUps_Script.Instance.GameOver_PopUp.SetActive(true);
            Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[6].Name);
            PopUps_Script.Instance.pause_Button.SetActive(false);
        }
    }
}
